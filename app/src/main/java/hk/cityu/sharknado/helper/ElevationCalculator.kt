package hk.cityu.sharknado.helper

import android.util.Log


/**
 * Created by Matej Danicek on 23.7.18.
 */
class ElevationCalculator {

    companion object {
        var MAX_FROM_SENSOR = 1.5f

        fun orientationValueToSeekbarValue(orientation: Float): Int {
            Constants.TAG_INPUT?.let { Log.d(Constants.TAG_INPUT, "orientation: $orientation") }
            if (orientation < MAX_FROM_SENSOR * -1 || orientation > MAX_FROM_SENSOR) { //dont calculate for values coming from wrong orientation
                return 0
            }

            var scaledInt = -(ScaleUtils.scale(orientation, MAX_FROM_SENSOR, Constants.ELEVATION_POWER_SCALE)).toInt() // -100 <-> 100
            val signum = Math.signum(scaledInt.toDouble()).toInt()

//            val resultScale = Constants.ELEVATION_POWER_SCALE_POWERED
//            scaledInt = Math.pow(scaledInt.toDouble(), Constants.ELEVATION_EXPONENT).toInt() // 0 <-> 10000
            val resultScale : Int = Constants.ELEVATION_POWER_SCALE.toInt()
            scaledInt = ScaleUtils.scale(scaledInt, resultScale, Constants.SCALE_SEEKBAR) // -100 <-> 100

            scaledInt = Math.abs(scaledInt) * signum // -10000 <-> 10000
            return scaledInt
        }
    }
}