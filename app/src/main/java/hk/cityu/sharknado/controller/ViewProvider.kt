package hk.cityu.sharknado.controller

import hk.cityu.sharknado.model.InputDTO


/**
 * Created by Matej Danicek on 18.7.18.
 */
interface ViewProvider {

    fun getCurrentInput(): InputDTO?
    fun updateStatus()
}