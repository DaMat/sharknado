#include <SoftwareSerial.h>
SoftwareSerial BT(0, 1);
//SoftwareSerial BT(4,5); //RX, TX

int const CTRL_MOVE = 69;
int const CTRL_STOP = 70;

char const START = 's';
char const END = 'e';

int LEFT_PWR = 9;
int MID_PWR = 10;
int RIGHT_PWR = 11;

int LEFT_DIR = 7;
int MID_DIR = 8;
int RIGHT_DIR = 12;

int LED = 13;

void setup() {
  Serial.begin(9600);
  BT.begin(9600);

  pinMode(LEFT_PWR, OUTPUT);
  pinMode(MID_PWR, OUTPUT);
  pinMode(RIGHT_PWR, OUTPUT);
  pinMode(LEFT_DIR, OUTPUT);
  pinMode(MID_DIR, OUTPUT);
  pinMode(RIGHT_DIR, OUTPUT);

  pinMode(LED_BUILTIN, OUTPUT);
}

String data = "";
void loop() {
  if (BT.available() > 0) {
    char d = (char) BT.read();
    Serial.print("loop received: ");
    Serial.println((uint8_t) d);

    if (data.length() == 0 && d != START) {
      return;
    }

    if (d == END) {
      if (data.length() == 4) {
        handleMove(data);
        data = "";
      } else if (data.length() < 4) {
        data = data + d;
      }
    } else {
      if (data.length() > 4) {
        data = "";
      } else {
        data = data + d;
      }
    }
  }
}

void handleMove(String data) {
  Serial.println("handle move");
  //LEFT
  char left = data[1];
  bool leftDir = byteToDir(left);
  int leftPwr = byteToPower(left);

  //MIDDLE
  char mid = data[2];
  bool midDir = byteToDir(mid);
  int midPwr = byteToPower(mid);

  //RIGHT
  char right = data[3];
  bool rightDir = byteToDir(right);
  int rightPwr = byteToPower(right);

  changeDir(LEFT_DIR, leftDir);
  move(LEFT_PWR, leftPwr);
  changeDir(MID_DIR, midDir);
  move(MID_PWR, midPwr);
  changeDir(RIGHT_DIR, rightDir);
  move(RIGHT_PWR, rightPwr);
  Serial.print("moving, left: ");
  Serial.print(leftPwr);
  Serial.print(", middle: ");
  Serial.print(midPwr);
  Serial.print(", right: ");
  Serial.println(rightPwr);
}

void handleStop() {
  move(LEFT_PWR, 0);
  move(MID_PWR, 0);
  move(RIGHT_PWR, 0);
}

bool byteToDir(char value) {
  return (((int) value & 128) > 0);
}

int byteToPower(char value) {
  return ((int) value & 127) * 2;
}

void move(int pin, uint8_t power) {
  analogWrite(pin, power);
}

void changeDir(int pin, bool dir) {
  if (dir) {
    digitalWrite(pin, HIGH);
  } else {
    digitalWrite(pin, LOW);
  }
}




//  byte t1 = 190;
//  bool t1Dir = byteToDir(t1);
//  Serial.println(t1Dir);
//  int t1Pwr = byteToPower(t1);
//  Serial.println(t1Pwr);
//  
//  t1 = 90;
//  t1Dir = byteToDir(t1);
//  Serial.println(t1Dir);
//  t1Pwr = byteToPower(t1);
//  Serial.println(t1Pwr);




//    Serial.println(rec);
//    switch (rec) {
//      case 'a' : {
//        Serial.println("a");
//        move(LEFT_PWR, 127);
//        changeDir(LEFT_DIR, true);
//        break;
//      }
//      case 'b': {
//        Serial.println("b");
//        move(LEFT_PWR, 0);
//        break;
//      }
//      case 'c': {
//        Serial.println("c");
//        move(LEFT_PWR, 127);
//        changeDir(LEFT_DIR, false);
//        break;
//      }
//    }


