@file:JvmName("Constants")

package hk.cityu.sharknado.helper

import android.view.View


/**
 * Created by Matej Danicek on 19.7.18.
 */
object Constants {
    const val TAG_MANAGER = "ConnectionManager"
    const val TAG_CONNECTION = "BLE_Connection"
    //    const val TAG_INPUT = "User_input"
    val TAG_INPUT: String? = null

    /**
     * VISIBLE if views visible only when connected should be visible always for debug purposes
     */
//    const val DEBUG_INVISIBLE = View.VISIBLE
    const val DEBUG_INVISIBLE = View.INVISIBLE

    const val MAC = "20:91:48:BC:AF:BA"
    const val TICK = 200L

    const val REQ_ENABLE_BT = 90

//    const val SCALE_POWER = 15
    const val SCALE_POWER = 127
    const val SCALE_SEEKBAR = SCALE_POWER
    const val SCALE_TEXTVIEW = 100

    const val ELEVATION_EXPONENT: Double = 2.0
    const val ELEVATION_POWER_SCALE = 100.0f
    val ELEVATION_POWER_SCALE_POWERED = Math.pow(ELEVATION_POWER_SCALE.toDouble(), Constants.ELEVATION_EXPONENT).toInt()

    const val INSTRUCTION_DELIMETER = 128
//    const val DIRECTION_BACK = 16
    const val ID_LEFT = 0
    const val ID_MID = 32
    const val ID_RIGHT = 64
    const val ID_CONTROL = ID_MID + ID_RIGHT


//    val BYTES_STOP = byteArrayOf((INSTRUCTION_DELIMETER + ID_CONTROL).toByte())

    const val DIRECTION_BACK = 128
    const val BYTE_START = 's'.toByte()
    const val BYTE_END = 'e'.toByte()
    const val BYTE_ZERO = 0.toByte()
    val BYTES_STOP = byteArrayOf(BYTE_START, BYTE_ZERO, BYTE_ZERO, BYTE_ZERO, BYTE_END)
}