#include <SoftwareSerial.h>
SoftwareSerial BT(0, 1);
//SoftwareSerial BT(4,5); //RX, TX

int const INSTRUCTION_DELIMETER = 128;
int const ID_LEFT = 0;
int const ID_MID = 32;
int const ID_RIGHT = 64;
int ID_CTRL = 64 + 32;

int MOTOR_ID_MASK = 64 + 32;
int MOTOR_POWER_MASK = 1 + 2 + 4 + 8;
int const MOTOR_DIR_MASK = 16;
int const POWER_MULTIPLIER = 17;

int LEFT_PWR = 9;
int MID_PWR = 10;
int RIGHT_PWR = 11;

int LEFT_DIR = 7;
int MID_DIR = 8;
int RIGHT_DIR = 12;

int LED = 13;

void setup() {
  Serial.begin(9600);
  BT.begin(9600);

  pinMode(LEFT_PWR, OUTPUT);
  pinMode(MID_PWR, OUTPUT);
  pinMode(RIGHT_PWR, OUTPUT);
  pinMode(LEFT_DIR, OUTPUT);
  pinMode(MID_DIR, OUTPUT);
  pinMode(RIGHT_DIR, OUTPUT);

  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  if (BT.available() > 0) {
    uint8_t value = (uint8_t) BT.read();

    bool handle = isInstruction(value);
    Serial.print("loop received: ");
    Serial.print(value);
    if (handle) {
       Serial.println(" - is instruction");
       handleInstruction(value);
    } else {
       Serial.println(" - ignored");
    }
  }

}

bool isInstruction(uint8_t value) {
  return value >= INSTRUCTION_DELIMETER;
}

void handleInstruction(char value) {
  int motorId = byteToMotorId(value);
  int power = byteToPower(value);
  
  if (motorId == ID_CTRL) {
    switch (power) {
      case(0): {
        handleStop();
        break;
      }
    }
    return;
  }

  bool dir = byteToDir(value);
  int powerPin = 0;
  int dirPin = 0;
  switch (motorId) {
    case ID_LEFT: {
      powerPin = LEFT_PWR;
      dirPin = LEFT_DIR;
      Serial.print("left power: ");
      break;
    }
    case ID_MID: {
      powerPin = MID_PWR;
      dirPin = MID_DIR;
      Serial.print("mid power: ");
      break;
    }
    case ID_RIGHT: {
      powerPin = RIGHT_PWR;
      dirPin = RIGHT_DIR;
      Serial.print("right power: ");
      break;
    }
  }

  if (powerPin != 0 && dirPin != 0) {
    changeDir(dirPin, dir);
    move(powerPin, power);
    Serial.print(power);
  }
  Serial.println("");
}

int byteToMotorId(char value) {
  return ((int) value) & MOTOR_ID_MASK;
}

bool byteToDir(char value) {
  return (((int) value & MOTOR_DIR_MASK) > 0);
}

int byteToPower(char value) {
  return ((int) value & MOTOR_POWER_MASK) * POWER_MULTIPLIER;
}

void handleStop() {
  move(LEFT_PWR, 0);
  move(MID_PWR, 0);
  move(RIGHT_PWR, 0);
  Serial.print("stopped");
}
       
void move(int pin, uint8_t power) {
  analogWrite(pin, power);
}

void changeDir(int pin, bool dir) {
  if (dir) {
    digitalWrite(pin, HIGH);
  } else {
    digitalWrite(pin, LOW);
  }
}

