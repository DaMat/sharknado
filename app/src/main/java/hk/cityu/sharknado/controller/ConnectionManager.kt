package hk.cityu.sharknado.controller

import android.content.Context
import android.os.Handler
import android.util.Log
import hk.cityu.sharknado.helper.Constants
import hk.cityu.sharknado.model.InputDTO


/**
 * Created by Matej Danicek on 18.7.18.
 */
class ConnectionManager private constructor() {
    private var viewProvider: ViewProvider? = null
    var connection: BLEConnection? = null

    var connecting = false
    var ticking = false
    var runnable: Runnable? = null
    private val handler: Handler = Handler()

    private object Holder {
        val INSTANCE = ConnectionManager()
    }

    companion object {
        val instance: ConnectionManager by lazy { Holder.INSTANCE }
    }


    fun register(viewProvider: ViewProvider) {
        this.viewProvider = viewProvider
        Constants.TAG_MANAGER?.let { Log.d(Constants.TAG_MANAGER, "registered") }
    }

    /**
     * Tries sending the stop command then stops the ticking and nulls the provider reference
     */
    fun unregister() {
        sendStopCommandIfPossible()
        stopTickSending()
        viewProvider?.updateStatus()
        this.viewProvider = null
        Constants.TAG_MANAGER?.let { Log.d(Constants.TAG_MANAGER, "unregistered") }
    }

    /**
     * Starts the periodical command sending. Does nothing and returns false if data cannot be sent to BLE ATM
     */
    fun startTickSending(): Boolean {
        if (connection?.canSendData() != true) {
            Constants.TAG_MANAGER?.let { Log.d(Constants.TAG_MANAGER, "tick not started - not connected") }
            return false
        }

        if (ticking) {
            stopTickSending()
        }

        runnable = Runnable {
            if (ticking) {
                Constants.TAG_MANAGER?.let { Log.d(Constants.TAG_MANAGER, "tick") }
                connection?.sendData(InputDTO.toBytes(viewProvider?.getCurrentInput()))
                handler.postDelayed(runnable, Constants.TICK)
            }
        }

        ticking = true
        handler.post(runnable)
        Constants.TAG_MANAGER?.let { Log.d(Constants.TAG_MANAGER, "tick started") }
        return true
    }

    fun stopTickSending() {
        ticking = false
        handler.removeCallbacks(runnable)
        Constants.TAG_MANAGER?.let { Log.d(Constants.TAG_MANAGER, "tick stopped") }

    }

    fun sendStopCommandIfPossible() {
        if (connection?.canSendData() == true) {
            if (connection?.sendData(Constants.BYTES_STOP) == true) {
                Constants.TAG_MANAGER?.let { Log.d(Constants.TAG_MANAGER, "stop sent") }
            }
        } else {
            Constants.TAG_MANAGER?.let { Log.d(Constants.TAG_MANAGER, "stop not sent") }
        }
    }

    fun isConnected(): Boolean {
        return connection?.canSendData() ?: false
    }

    /**
     * @return TRUE - if already connected or connecting has begun
     */
    fun connectIfNeeded(context: Context): Boolean {
        if (isConnected() || connecting) {
            Constants.TAG_MANAGER?.let { Log.d(Constants.TAG_MANAGER, "already connected or connecting") }
            return true
        }

        if (connection == null) {
            connection = BLEConnection()
            Constants.TAG_MANAGER?.let { Log.d(Constants.TAG_MANAGER, "new connection instance created") }
        }

        connecting = connection?.connect(context, Constants.MAC, object : BLEConnection.ConnectionChangedListener {
            override fun onConnectionChanged(connected: Boolean) {
                Constants.TAG_MANAGER?.let { Log.d(Constants.TAG_MANAGER, "connection changed, connected = $connected") }
                connecting = false
                if (!connected) {
                    stopTickSending()
                }
                viewProvider?.updateStatus()
            }
        }) ?: false
        Constants.TAG_MANAGER?.let { Log.d(Constants.TAG_MANAGER, "connecting..., success = $connecting") }
        return connecting
    }

    /**
     * Tries to send the stop command bytes and then disconnect the BLE connection
     */
    fun disconnect() {
        sendStopCommandIfPossible()
        stopTickSending()
        connection?.disconnect()
        connection = null
        Constants.TAG_MANAGER?.let { Log.d(Constants.TAG_MANAGER, "disconnected") }
    }
}