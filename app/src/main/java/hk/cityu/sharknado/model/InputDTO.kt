package hk.cityu.sharknado.model

import hk.cityu.sharknado.helper.Constants
import hk.cityu.sharknado.helper.ScaleUtils


/**
 * Created by Matej Danicek on 18.7.18.
 */
class InputDTO {
    var left: Int = 0
    var elevation: Int = 0
    var right: Int = 0

    companion object {
//        fun toBytes(inputDTO: InputDTO?): ByteArray {
//            if (inputDTO != null) {
//                val bytes = ByteArray(3)
//                bytes[0] = seekbarValueToByte(inputDTO.left, Constants.ID_LEFT)
//                bytes[1] = seekbarValueToByte(inputDTO.elevation, Constants.ID_MID)
//                bytes[2] = seekbarValueToByte(inputDTO.right, Constants.ID_RIGHT)
//
//                return bytes
//            } else {
//                return Constants.BYTES_STOP
//            }
//        }
//
//        fun seekbarValueToByte(value: Int, motorId : Int): Byte {
//            var powerValue: Int
//            powerValue = value - Constants.SCALE_SEEKBAR
//            powerValue = ScaleUtils.scale(powerValue, Constants.SCALE_SEEKBAR, Constants.SCALE_POWER)
//
//            var byteValue = Constants.INSTRUCTION_DELIMETER
//            if (powerValue < 0) {
//                byteValue += Constants.DIRECTION_BACK
//            }
//
//            byteValue += Math.abs(powerValue)
//            byteValue += motorId
//
//            return byteValue.toByte()
//        }

        fun toBytes(inputDTO: InputDTO?): ByteArray {
            if (inputDTO != null) {
                val bytes = ByteArray(5)
                bytes[0] = Constants.BYTE_START
                bytes[1] = seekbarValueToByte(inputDTO.left)
                bytes[2] = seekbarValueToByte(inputDTO.elevation)
                bytes[3] = seekbarValueToByte(inputDTO.right)
                bytes[4] = Constants.BYTE_END

                return bytes
            } else {
                return Constants.BYTES_STOP
            }
        }

        fun seekbarValueToByte(value: Int): Byte {
            var powerValue: Int
            powerValue = value - Constants.SCALE_SEEKBAR
            powerValue = ScaleUtils.scale(powerValue, Constants.SCALE_SEEKBAR, Constants.SCALE_POWER)

            var byteValue = 0
            if (powerValue < 0) {
                byteValue += Constants.DIRECTION_BACK
            }

            byteValue += Math.abs(powerValue)

            return byteValue.toByte()
        }
    }
}