package hk.cityu.sharknado.view

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.SeekBar
import com.polyak.iconswitch.IconSwitch
import hk.cityu.sharknado.R
import hk.cityu.sharknado.controller.ConnectionManager
import hk.cityu.sharknado.controller.ViewProvider
import hk.cityu.sharknado.helper.Constants
import hk.cityu.sharknado.helper.ElevationCalculator
import hk.cityu.sharknado.helper.ScaleUtils
import hk.cityu.sharknado.model.InputDTO
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), ViewProvider, SensorEventListener {
    private var mSensorManager: SensorManager? = null
    private val mAccelerometerReading = FloatArray(3)
    private val mMagnetometerReading = FloatArray(3)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        window.decorView.setBackgroundColor(ContextCompat.getColor(this, R.color.activity_background))

//        window.setFlags(
//                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
//                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
//        )

        seekbarLeft.max = Constants.SCALE_SEEKBAR * 2
        seekbarRight.max = Constants.SCALE_SEEKBAR * 2
        seekbarElevation.max = Constants.SCALE_SEEKBAR * 2
        seekbarElevation.isEnabled = false

        seekbarLeft.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
            }

            override fun onProgressChanged(seekBar: SeekBar?, p1: Int, p2: Boolean) {
                textLeft.text = seekbarProgressToPercent(seekBar?.progress ?: 0)
            }
        })

        seekbarRight.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
            }

            override fun onProgressChanged(seekBar: SeekBar?, p1: Int, p2: Boolean) {
                textRight.text = seekbarProgressToPercent(seekBar?.progress ?: 0)
            }
        })

        seekbarElevation.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
            }

            override fun onProgressChanged(seekBar: SeekBar?, p1: Int, p2: Boolean) {
                textElevation.text = seekbarProgressToPercent(seekBar?.progress ?: 0)
            }
        })

        updateSwitchFly()

        switchElevation.setCheckedChangeListener(object : IconSwitch.CheckedChangeListener {
            override fun onCheckChanged(current: IconSwitch.Checked?) {
                seekbarElevation.isEnabled = current == IconSwitch.Checked.RIGHT
            }
        })

        resetProgress()

        var systemService = getSystemService(Context.SENSOR_SERVICE)
        if (systemService is SensorManager) {
            mSensorManager = systemService
        }
    }

    override fun onResume() {
        super.onResume()
        ConnectionManager.instance.register(this)

        updateStatus()
        switchFly.setCheckedChangeListener(object : IconSwitch.CheckedChangeListener {
            override fun onCheckChanged(current: IconSwitch.Checked?) {
                goFlyToggledByUser()
            }
        })

        mSensorManager?.registerListener(this, mSensorManager?.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL, SensorManager.SENSOR_DELAY_UI)
        mSensorManager?.registerListener(this, mSensorManager?.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_NORMAL, SensorManager.SENSOR_DELAY_UI)
    }

    override fun onPause() {
        super.onPause()
        switchFly.setCheckedChangeListener(null)
        mSensorManager?.unregisterListener(this)
        ConnectionManager.instance.unregister()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constants.REQ_ENABLE_BT && resultCode == Activity.RESULT_OK) {
            connectClick(btnConnect)
        }
    }

    override fun getCurrentInput(): InputDTO? {
        var inputDTO = InputDTO()
        inputDTO.left = seekbarLeft.progress
        inputDTO.right = seekbarRight.progress
        inputDTO.elevation = seekbarElevation.progress

        Constants.TAG_INPUT?.let { Log.d(Constants.TAG_INPUT, "new input:") }
        Constants.TAG_INPUT?.let { Log.d(Constants.TAG_INPUT, "left ${seekbarLeft.progress}") }
        Constants.TAG_INPUT?.let { Log.d(Constants.TAG_INPUT, "middle ${seekbarElevation.progress}") }
        Constants.TAG_INPUT?.let { Log.d(Constants.TAG_INPUT, "right ${seekbarRight.progress}") }

        return inputDTO
    }

    override fun updateStatus() {
        //must be run on UI thread because the callback might be called from background thread
        runOnUiThread {
            if (ConnectionManager.instance.connecting) {
                textStatus.text = getString(R.string.connecting)
                btnConnect.isEnabled = false
                switchFly.visibility = Constants.DEBUG_INVISIBLE
                switchElevation.visibility = Constants.DEBUG_INVISIBLE
            } else {
                if (ConnectionManager.instance.isConnected()) {
                    textStatus.text = getString(R.string.connected)
                    btnConnect.setImageDrawable(getDrawable(R.drawable.ic_bt_on))
                    switchFly.visibility = View.VISIBLE
                    switchElevation.visibility = View.VISIBLE
                } else {
                    textStatus.text = getString(R.string.disconnected)
                    btnConnect.setImageDrawable(getDrawable(R.drawable.ic_bt_off))
                    switchFly.visibility = Constants.DEBUG_INVISIBLE
                    switchElevation.visibility = Constants.DEBUG_INVISIBLE
                }
                btnConnect.isEnabled = true
            }

            updateSwitchFly()
        }
    }

    fun updateSwitchFly() {
        if (ConnectionManager.instance.ticking) {
            switchFly.checked = IconSwitch.Checked.RIGHT
        } else {
            switchFly.checked = IconSwitch.Checked.LEFT
        }
    }

    fun btNeedsStarting(): Boolean {
        val mBluetoothAdapter: BluetoothAdapter?
        val systemService = getSystemService(Context.BLUETOOTH_SERVICE)

        if (systemService is BluetoothManager) {
            mBluetoothAdapter = systemService.adapter

            // Ensures Bluetooth is available on the device and it is enabled. If not,
            // displays a dialog requesting user permission to enable Bluetooth.
            if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled) {
                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                ActivityCompat.startActivityForResult(this, enableBtIntent, Constants.REQ_ENABLE_BT, null)
                return true
            }
        }

        return false
    }

    fun connectClick(view: View) {
        if (btNeedsStarting()) {
            return
        }

        if (ConnectionManager.instance.isConnected()) {
            ConnectionManager.instance.disconnect()
        } else {
            ConnectionManager.instance.connectIfNeeded(this)
        }
        updateStatus()
    }

    fun goFlyToggledByUser() {
        if (!ConnectionManager.instance.isConnected()) {
            return
        }

        if (ConnectionManager.instance.ticking) {
            ConnectionManager.instance.sendStopCommandIfPossible()
            ConnectionManager.instance.stopTickSending()
        } else {
            ConnectionManager.instance.startTickSending()
        }
        updateStatus()
        resetProgress()
    }

    fun resetProgress() {
        seekbarLeft.progress = Constants.SCALE_SEEKBAR
        seekbarRight.progress = Constants.SCALE_SEEKBAR
        if (switchElevation.checked == IconSwitch.Checked.RIGHT) seekbarElevation.progress = Constants.SCALE_SEEKBAR
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
        //ignored
    }

    override fun onSensorChanged(event: SensorEvent?) {
        if (event == null || switchElevation.checked == IconSwitch.Checked.RIGHT) return
        if (event.sensor.type == Sensor.TYPE_ACCELEROMETER) {
            System.arraycopy(event.values, 0, mAccelerometerReading, 0, mAccelerometerReading.size)
        } else if (event.sensor.type == Sensor.TYPE_MAGNETIC_FIELD) {
            System.arraycopy(event.values, 0, mMagnetometerReading, 0, mMagnetometerReading.size)
        }

        val rotationMatrix = FloatArray(9)
        SensorManager.getRotationMatrix(rotationMatrix, null, mAccelerometerReading, mMagnetometerReading)

        // Express the updated rotation matrix as three orientation angles.
        val orientationAngles = FloatArray(3)
        SensorManager.getOrientation(rotationMatrix, orientationAngles)

        // 0 -> 1.5 = down
        // 0 -> -1.5 = up

        var elevation = ElevationCalculator.orientationValueToSeekbarValue(orientationAngles[2])
        elevation += Constants.SCALE_SEEKBAR
        seekbarElevation.progress = elevation
        Constants.TAG_INPUT?.let { Log.d(Constants.TAG_INPUT, "orientation: $elevation") }
    }

    fun seekbarProgressToPercent(progress: Int): String {
        val number = ScaleUtils.scale(progress - Constants.SCALE_SEEKBAR, Constants.SCALE_SEEKBAR, Constants.SCALE_TEXTVIEW)
        return "$number%"
    }
}
