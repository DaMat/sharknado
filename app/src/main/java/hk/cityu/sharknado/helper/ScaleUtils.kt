package hk.cityu.sharknado.helper


/**
 * Created by Matej Danicek on 24.7.18.
 */
class ScaleUtils {

    companion object {
        fun scale(value : Float, oldMax : Float, newMax : Float) : Float {
            return (value / oldMax) * newMax
        }

        fun scale(value : Int, oldMax : Int, newMax : Int) : Int {
            return scale(value.toFloat(), oldMax.toFloat(), newMax.toFloat()).toInt()
        }

//        fun scale(value : Int, oldMax : Int, newMax : Int, includeMax : Boolean) : Int {
//            var newValue = scale(value, oldMax, newMax)
//            if (!includeMax && newValue == newMax) {
//                newValue--
//            }
//            return newValue
//        }
    }
}